cmake_minimum_required(VERSION 3.1)

project(3D VERSION 0.1)

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

cmake_policy (SET CMP0072 NEW)
find_package(OpenGL REQUIRED)


add_subdirectory(lib/glad_repo)
add_subdirectory(lib/glfw)
add_subdirectory(src/)
