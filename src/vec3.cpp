#include<iostream>
#include<fstream>
#include<stdint.h>
#include<string.h>

struct vec3 
{
	public:
		uint32_t x, y, z;
			
	static void fromString(const std::string s, vec3& vec){
		size_t i,j;
		i = s.find(" ");
		j = s.find(" ", i + 1);

		vec.x = std::stoi(s.substr(0, i));
		vec.y = std::stoi(s.substr(i+1, j - i));
		vec.z = std::stoi(s.substr(j+1, std::string::npos));

		//std::cout << s.substr(0, i) << std::endl;
		//std::cout << s.substr(i+1, j - i) << std::endl;	
		//std::cout << s.substr(j+1, std::string::npos) << std::endl;	
	}

};
