#include<iostream>
#include<string.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>


const char *vertex_shader_source = 
	"#version 330 core\n"
    "layout (location = 0) in vec3 p;\n"
    "void main()\n"
    "{\n"
    "   gl_Position = vec4(p.x, p.y, p.z, 1.0);\n"
    "}\0";

const char *frag_shader_source =
	"#version 330 core\n"
	"out vec4 FragColor;\n"

	"void main()\n"
    "{\n"
    "	FragColor = vec4(1.0f, 1.0f, 0.8f, 1.0f);\n"
	"}\0";

int main(int argc, char* args[]){
	GLFWwindow* window;

	if (!glfwInit())
        return -1;

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		
 	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

	glfwMakeContextCurrent(window);

	if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)){
        printf("Something went wrong!\n");
        exit(-1);
    }
	printf("OpenGL %d.%d\n", GLVersion.major, GLVersion.minor);
	glViewport(0, 0, 640, 480);

	//shader
	GLuint vertex_shader;
	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
	glCompileShader(vertex_shader);	

	GLuint frag_shader;
	frag_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag_shader, 1, &frag_shader_source, NULL);
	glCompileShader(frag_shader);
	

	int success;
    char infoLog[512];
 	glGetShaderiv(frag_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(frag_shader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }


	GLuint shader_prog;
	shader_prog = glCreateProgram();	
	glAttachShader(shader_prog, vertex_shader);
	glAttachShader(shader_prog, frag_shader);
	glLinkProgram(shader_prog);

	glDeleteShader(vertex_shader);
	glDeleteShader(frag_shader); 


	float tri_pos[] = {
		-0.5f, -0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		 0.0f,  0.5f, 0.0f
	};  

	GLuint VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(tri_pos), tri_pos, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

	//unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0); 
    glBindVertexArray(0); 

	
	//array and buffer

    while (!glfwWindowShouldClose(window))
    {
        //* Render here *
		glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shader_prog);
        glBindVertexArray(VAO); 
        glDrawArrays(GL_TRIANGLES, 0, 3);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

	glfwTerminate();
    return 0;
}
